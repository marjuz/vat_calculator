﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CreateInvoice.DataContract
{
    public class Invoice
    {
        public string Id { get; set; }
        public string InvoiceNumber { get; set; }
        public string OrderId { get; set; }
        public string Description { get; set; }
        public double VAT { get; set; }
        public double TotalAmount { get; set; }
    }
}
