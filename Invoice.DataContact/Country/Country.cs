﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CreateInvoice.DataContract
{
    public class Country
    {
        public Country(string countryCode)
        {
            CountryCode = countryCode;
        }

        public string CountryCode { get; }
        public string CountryName { get; set; }
    }
}
