﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CreateInvoice.DataContract
{
    public class Customer
    {
      
        public string Id { get; set; }
        public string CustomerNumber { get; set; }
        public string CustomerName { get; set; }
        public string CustomerSurname { get; set; }
        public string VATNumber { get; set; }
        public bool IsVAT { get { return (VATNumber != ""); } }
        public bool IsCompany { get { return (CustomerSurname == null); } }
        public string Description { get; set; }
        public Country CustomerCountry { get; set; }
    }
}
