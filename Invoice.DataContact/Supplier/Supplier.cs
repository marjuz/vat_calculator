﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CreateInvoice.DataContract
{
    public class Supplier
    {

        public string Id { get; set; }
        public string SupplierNumber { get; set; }
        public string SupplierName { get; set; }
        public string SupplierSurname { get; set; }
        public string VATNumber { get; set; }
        public bool IsVAT { get { return (VATNumber != ""); } }
        public bool IsCompany { get { return (SupplierSurname == null); } }
        public string Description { get; set; }
        public Country SupplierCountry { get; set; }

    }
}
