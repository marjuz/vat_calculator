﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CreateInvoice.DataContract
{
    public class Order
    {

        public string Id { get; set; }
        public string OrderNum { get; set; }
        public string SupplierId { get; set; }
        public string CustomerId { get; set; }
        public double Amount { get; set; }  // number value
        public double Discount { get; set; } // % value
        public double ShipCharge { get; set; } // number value
        public double TotalAmount { get { return (Amount * (1 - Discount / 100)) + ShipCharge; } }
        public double TotalPaid { get; set; }  // number value 
        public string CurencyCode { get; set; }
        public string Description { get; set; }
    }
}
