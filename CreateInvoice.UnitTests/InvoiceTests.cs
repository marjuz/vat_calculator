using Microsoft.VisualStudio.TestTools.UnitTesting;
using CreateInvoice.DataContract;
using CreateInvoice.Service;
using NSubstitute;
using System;

namespace UnitTests
{
    [TestClass]
    public class InvoiceTests
    {
        private IAPIServices _APIServices;
        private IInvoiceService _invoiceService;

        public InvoiceTests()
        {
            _APIServices = Substitute.For<IAPIServices>();
        }

        [TestMethod]
        public void VatCalculation_SupplierNotVatPayer_Returns0()
        {
            //Data
            var _supplier = new Supplier() { VATNumber = "", SupplierCountry = new Country("LT")};
            var _customer = new Customer() { VATNumber = "", CustomerCountry = new Country("LT") };
            var _order = new Order() { Amount = 1000, Discount = 5, ShipCharge = 50};

            _APIServices.Vat(Arg.Any<string>()).Returns(0);
            _APIServices.IsInEu(Arg.Any<string>()).Returns(true);

            _invoiceService = new InvoiceService(_APIServices);

            //Call
            var result = _invoiceService.CreateInvoice(_supplier, _customer, _order);
            //Assert
            Assert.AreEqual(result.TotalAmount,1000);
            Assert.AreEqual(result.VAT, 0);
        }

        [TestMethod]
        public void VatCalculation_SupplierVatPayerCustomerNotEu_Returns0()
        { 
            //Data
            var _supplier = new Supplier() { VATNumber = "LT151515", SupplierCountry = new Country("LT") };
            var _customer = new Customer() { VATNumber = "", CustomerCountry = new Country("BL") };
            var _order = new Order() { Amount = 1000, Discount = 5, ShipCharge = 50 };

            _APIServices.Vat(Arg.Any<string>()).Returns(0);
            _APIServices.IsInEu(Arg.Any<string>()).Returns(false);

            _invoiceService = new InvoiceService(_APIServices);

            //Call
            var result = _invoiceService.CreateInvoice(_supplier, _customer, _order);

            //Assert
            Assert.AreEqual(result.TotalAmount, 1000);
            Assert.AreEqual(result.VAT, 0);
        }

        [TestMethod]
        public void VatCalculation_SupplierVatPayerCustomerEuVatPayerNotSameCounty_Returns0()
        {

            //Data
            var _supplier = new Supplier() { VATNumber = "LT151515", SupplierCountry = new Country("LT") };
            var _customer = new Customer() { VATNumber = "FR454545", CustomerCountry = new Country("FR") };
            var _order = new Order() { Amount = 1000, Discount = 5, ShipCharge = 50 };

            _APIServices.Vat(Arg.Any<string>()).Returns(0);
            _APIServices.IsInEu(Arg.Any<string>()).Returns(true);

            _invoiceService = new InvoiceService(_APIServices);

            //Call
            var result = _invoiceService.CreateInvoice(_supplier, _customer, _order);

            //Assert
            Assert.AreEqual(result.TotalAmount, 1000);
            Assert.AreEqual(result.VAT, 0);
        }

        [TestMethod]
        public void VatCalculation_SupplierVatPayerCustomerEuNotVatPayerNotSameCounty_ReturnsCustomerVat20()
        {
            //Data
            var _supplier = new Supplier() { VATNumber = "LT151515", SupplierCountry = new Country("LT") };
            var _customer = new Customer() { VATNumber = "", CustomerCountry = new Country("SK") };
            var _order = new Order() { Amount = 1000, Discount = 5, ShipCharge = 50 };

            _APIServices.Vat(Arg.Any<string>()).Returns(20);
            _APIServices.IsInEu(Arg.Any<string>()).Returns(true);

            _invoiceService = new InvoiceService(_APIServices);

            //Call
            var result = _invoiceService.CreateInvoice(_supplier, _customer, _order);

            //Assert
            Assert.AreEqual(result.TotalAmount, 1200);
            Assert.AreEqual(result.VAT, 20);
        }

        [TestMethod]
        public void VatCalculation_SupplierVatPayerCustomerEuNotVatPayerSameCounty_ReturnsCustomerVat21()
        {
            //Data
            var _supplier = new Supplier() { VATNumber = "LT151515", SupplierCountry = new Country("LT") };
            var _customer = new Customer() { VATNumber = "", CustomerCountry = new Country("LT") };
            var _order = new Order() { Amount = 1000, Discount = 5, ShipCharge = 50 };

            _APIServices.Vat(Arg.Any<string>()).Returns(21);
            _APIServices.IsInEu(Arg.Any<string>()).Returns(true);

            _invoiceService = new InvoiceService(_APIServices);

            //Call
            var result = _invoiceService.CreateInvoice(_supplier, _customer, _order);

            //Assert
            Assert.AreEqual(result.TotalAmount, 1210);
            Assert.AreEqual(result.VAT, 21);
        }

        [TestMethod]
        public void VatCalculation_SupplierVatPayerCustomerEuVatPayerSameCounty_ReturnsCustomerVat21()
        {
            //Data
            var _supplier = new Supplier() { VATNumber = "LT151515", SupplierCountry = new Country("LT") };
            var _customer = new Customer() { VATNumber = "LT303030", CustomerCountry = new Country("LT") };
            var _order = new Order() { Amount = 1000, Discount = 5, ShipCharge = 50 };

            _APIServices.Vat(Arg.Any<string>()).Returns(21);
            _APIServices.IsInEu(Arg.Any<string>()).Returns(true);

            _invoiceService = new InvoiceService(_APIServices);

            //Call
            var result = _invoiceService.CreateInvoice(_supplier, _customer, _order);
            //Assert
            Assert.AreEqual(result.TotalAmount, 1210);
            Assert.AreEqual(result.VAT, 21);
        }

    }
}
