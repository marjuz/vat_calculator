﻿using System;
using System.Collections.Generic;
using System.Text;
using CreateInvoice.DataContract;

namespace CreateInvoice.Service
{
    public interface IAPIServices
    {
        double Vat(string countryCode);
        bool IsInEu(string countryCode);
    }
}
