﻿using System;
using System.Collections.Generic;
using System.Text;
using CreateInvoice.DataContract;

namespace CreateInvoice.Service
{
    public class InvoiceService : IInvoiceService
    {
        private IAPIServices _VATValueAPIService;

        public InvoiceService(IAPIServices VATValueAPIService)
        {
            _VATValueAPIService = VATValueAPIService;
    }

        public Invoice CreateInvoice(Supplier supplier, Customer customer, Order order)
        {
            var VAT = _VATValueAPIService.Vat(customer.CustomerCountry.CountryCode);
            var customerIsInEu = _VATValueAPIService.IsInEu(customer.CustomerCountry.CountryCode);

            var VATValue = (!supplier.IsVAT ||
                            !customerIsInEu ||
                            (customerIsInEu &&
                             customer.IsVAT &&
                             supplier.SupplierCountry.CountryCode != customer.CustomerCountry.CountryCode)) ? 
                            0 :
                            VAT;

            var newInvoice = new Invoice()
            {
                Id = Guid.NewGuid().ToString(),
                InvoiceNumber = Guid.NewGuid().ToString(), //API to generate specific number
                Description = string.Format("Invoice for Order No. {0}.", order.OrderNum),
                VAT = VATValue,
                OrderId = order.Id,
                TotalAmount = order.TotalAmount * (1 + VATValue / 100)
            };

            return newInvoice;
        }
    }
}
