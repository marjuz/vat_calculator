﻿using System;
using System.Collections.Generic;
using System.Text;
using CreateInvoice.DataContract;

namespace CreateInvoice.Service
{
    public interface IInvoiceService
    {
        Invoice CreateInvoice(Supplier supplier, Customer customer, Order order);
    }
}
